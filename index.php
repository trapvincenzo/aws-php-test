<?php

require_once 'vendor/autoload.php';

$redisConfig = require_once 'config/redis.php';
$predis = new Predis\Client([
	'host' => $redisConfig['host'],
	'port' => $redisConfig['port']
]);
$predis->set('testkey', 'test!');

print $predis->get('testkey');